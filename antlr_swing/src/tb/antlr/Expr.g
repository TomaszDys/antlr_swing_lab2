grammar Expr;
 
options {
  output=AST;
  ASTLabelType=CommonTree;
}
 
@header {
  package tb.antlr;
}
 
@lexer::header {
  package tb.antlr;
}
 
prog
    : (stat)+ EOF!;
 
stat
    : expr NL -> expr
 
    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
//    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | if_statement NL -> if_statement
    | PRINT expr NL -> ^(PRINT expr)
    | NL ->
    ;
 
if_statement
    : IF^ expr THEN! expr (ELSE! expr)?
    ;
 
expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;
 
multExpr
    : powExpr
        ( MUL^ powExpr
        | DIV^ powExpr
        | MOD^ powExpr
        )*
    ;
powExpr
    : atom (POWER^ powExpr)?
    ;
 
atom
    : INT
    | ID
    | LP! expr RP!
    ;
 
VAR :'var';
 
PRINT
    : 'print'
    ;
 
IF : 'if';
 
THEN : 'then';
 
ELSE : 'else';
    
ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;
 
INT : '0'..'9'+;
 
NL : '\r'? '\n' ;
 
WS : (' ' | '\t')+ {$channel = HIDDEN;} ;
 
 
LP
        :       '('
        ;
 
RP
        :       ')'
        ;
 
BLOCKSTART
    : '{'
    ;
 
BLOCKEND
    : '}'
    ;
 
PODST
        :       '='
        ;
 
PLUS
        :       '+'
        ;
 
MINUS
        :       '-'
        ;
 
MUL
        :       '*'
        ;
 
DIV
        :       '/'
        ;
MOD
  : '%'
  ;
 
POWER
    : '^'
    ;  
 
SL
    : '<<'
    ;   
 
SR
    : '>>'
    ;