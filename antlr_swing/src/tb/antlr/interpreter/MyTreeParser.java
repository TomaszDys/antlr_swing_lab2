package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
  GlobalSymbols globalSymbols = new GlobalSymbols();
  public MyTreeParser(TreeNodeStream input) {
      super(input);
  }

  public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
      super(input, state);
    }

  protected void drukuj(String text) {
      System.out.println(text.replace('\r', ' ').replace('\n', ' '));
  }

	protected int getInt(String text) {
		return Integer.parseInt(text);
	}

  	protected void addVar(String name) {
		globalSymbols.newSymbol(name);
	}
	
	protected int getVar(String name) {
		return globalSymbols.getSymbol(name);
	}
	
	protected void setVar(String name, int value) {
		globalSymbols.setSymbol(name, value);
  }	

  protected int division(int a, int b) throws ArithmeticException {
		if (b == 0) {
      throw new ArithmeticException("Devision by 0 is forbidden.");
    }
		return a / b;
	}

	protected int modulo(int a, int b) throws ArithmeticException {
		return a % b;
	}
	
	protected int power(int a, int b) {
		return (int)Math.pow(a, b);
	}
}
